import React from 'react';
import {
    Text,
    View,
    KeyboardAvoidingView,
    TextInput,
    ScrollView,
    StyleSheet,
    Button,
    Picker,
    Dimensions
} from 'react-native';

import SalvarDados from './SalvarDados';

export default class ListaValores extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            noteArray: [],
            noteText: '',
            noteDescription: '',
            tipoSelecionado: '',
            entradaOuSaida: '',
            valorBalanco: 0,

            mostrarBotão: false,
        };

        this.renderizarInput = this.renderizarInput.bind(this);

    }

    renderizarInput() {
        if (!this.state.mostrarBotão) {
            return (
                <Button
                    style={styles.addButton}
                    title="APERTE PARA ABRIR FORMULÁRIO DE ADIÇÃO DE VALOR"
                    onPress={() => this.setState({ mostrarBotão: true })}
                ></Button>
            );
        } else {
            return (
                
                    <KeyboardAvoidingView behavior="position" style={styles.footer}>
                        <View style={{ width: 25 }}>
                            <Button
                                title="X"
                                onPress={() => this.setState({ mostrarBotão: false })}
                            ></Button>
                        </View >
                        <TextInput
                            style={styles.textInput}
                            placeholder='>Valor'
                            placeholderTextColor='white'
                            underlineColorAndroid='transparent'
                            onChangeText={(noteText) => this.setState({ noteText })}
                            value={this.state.noteText}
                            keyboardType="number-pad"
                        >
                        </TextInput>

                        <TextInput
                            style={styles.textInput}
                            placeholder='>Descrição'
                            placeholderTextColor='white'
                            underlineColorAndroid='transparent'
                            onChangeText={(noteDescription) => this.setState({ noteDescription })}
                            value={this.state.noteDescription}
                            keyboardType="default"
                        >
                        </TextInput>

                        <Picker
                        selectedValue={this.state.tipoSelecionado}
                        style={{ height: 50, width: Dimensions.get('window').width, backgroundColor: 'white' }}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({ tipoSelecionado: itemValue })
                        }
                    >
                        <Picker.Item label="Aperte Para Selecionar a Categoria" value={null} />
                        <Picker.Item label="Supermercado" value="Supermercado" />
                        <Picker.Item label="Luz" value="Luz" />
                        <Picker.Item label="Água" value="Água" />
                        <Picker.Item label="Dízimo" value="Dízimo" />
                    </Picker>

                    <Picker

                        selectedValue={this.state.entradaOuSaida}
                        style={{ height: 50, width: Dimensions.get('window').width }}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({ entradaOuSaida: itemValue })
                        }
                    >
                        <Picker.Item label="Aperte Para Selecionar a Transação" value={null} />
                        <Picker.Item label="Entrada" value="true" />
                        <Picker.Item label="Saída" value="false" />
                    </Picker>

                    <Button
                        onPress={this.addNote.bind(this)}
                        title='APERTE AQUI PARA ADICIONAR O VALOR'
                    >
                    </Button>
                    </KeyboardAvoidingView >
                    
       
            );
        }
    }

    renderizarBalanco() {
        if (this.state.valorBalanco < 0) {
            return (
                <View style={{
                    height: 20,
                    width: Dimensions.get('window').width,
                    backgroundColor: '#ad0000',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <Text style={{ color: 'white', fontWeight: "bold" }}>Balanço Total: R$ {this.state.valorBalanco.toFixed(2)}</Text>
                </View>
            );
        } else if (this.state.valorBalanco >= 0) {
            return (
                <View style={{
                    height: 20,
                    width: Dimensions.get('window').width,
                    backgroundColor: '#068700',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <Text style={{ color: 'white', fontWeight: "bold" }}>Balanço Total: R$ {this.state.valorBalanco.toFixed(2)}</Text>
                </View>
            );
        }
    }

    render() {

        let notes = this.state.noteArray.map((val, key) => {
            return <SalvarDados
                key={key}
                keyVal={key}
                val={val}
                deleteMethod={() => this.deleteNote(key)}
            />
        });

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>- Finanças da Vovó -</Text>
                </View>

                <ScrollView style={styles.scrollContainer}>
                    {notes}
                </ScrollView>

                {this.renderizarBalanco()}

                {this.renderizarInput()}
            </View>
        );
    }

    addNote() {
        if (this.state.noteText) {

            this.setState({ mostrarBotão: false });
            var d = new Date();
            this.state.noteArray.push({
                'date': d.getDate() +
                    "/" + (d.getMonth() + 1) +
                    "/" + d.getFullYear(),
                'note': this.state.noteText,
                'description': this.state.noteDescription,
                'type': this.state.tipoSelecionado,
                'entradaOuSaida': this.state.entradaOuSaida,
            });
            this.setState({ noteArray: this.state.noteArray });
            this.setState({ noteText: '' });
            this.setState({ noteDescription: '' });
            this.setState({ tipoSelecionado: '' });
            this.setState({ entradaOuSaida: '' });

            if (this.state.entradaOuSaida == "true") {
                let parseDoValorInserido = this.state.noteText.replace(",", ".");
                let numero = parseFloat(parseDoValorInserido);
                let resultado = numero + this.state.valorBalanco;
                this.setState({ valorBalanco: resultado })
            } else {
                let parseDoValorInserido = this.state.noteText.replace(",", ".");
                let numero = parseFloat(parseDoValorInserido);
                let resultado = this.state.valorBalanco - numero;
                this.setState({ valorBalanco: resultado })
            }

        }
    }

    deleteNote(key) {
        let valores = this.state.noteArray.map((val, key) => {
            if (val.entradaOuSaida == "true") {
                let parseDoValNote = val.note.replace(",", ".");
                let valor = parseFloat(parseDoValNote);
                let resultado = this.state.valorBalanco - valor;
                this.setState({ valorBalanco: resultado })
            } else {
                let parseDoValNote = val.note.replace(",", ".");
                let valor = parseFloat(parseDoValNote);
                let resultado = this.state.valorBalanco + valor;
                this.setState({ valorBalanco: resultado })
            }
        });
        this.state.noteArray.splice(key, 1);
        this.setState({ noteArray: this.state.noteArray });

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#E91E63',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 5,
        borderBottomColor: '#ddd',
    },
    headerText: {
        color: 'white',
        fontSize: 18,
        padding: 26,
    },
    scrollContainer: {
        flex: 1,
        marginBottom: 100,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10,
        backgroundColor: 'white',
    },
    textInput: {
        alignSelf: 'stretch',
        color: '#fff',
        padding: 20,
        backgroundColor: '#252525',
        borderTopWidth: 2,
        borderTopColor: '#ededed',
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 90,
        backgroundColor: '#E91E63',
        width: 90,
        height: 90,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    }
});