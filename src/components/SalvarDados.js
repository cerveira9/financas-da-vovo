import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    AsyncStorage,
    Text
} from 'react-native';

export default class SalvarDados extends React.Component {

    renderCorEntradaOuSaida() {
        if (this.props.val.entradaOuSaida == "true") {
            return (
                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: 1 }}>
                        <Text style={styles.noteTextEntrada}>{this.props.val.date}</Text>
                        <Text style={styles.noteTextEntrada}>{this.props.val.note}</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text>{this.props.val.description}</Text>
                        <Text>{this.props.val.type}</Text>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: 1 }}>
                        <Text style={styles.noteText}>{this.props.val.date}</Text>
                        <Text style={styles.noteText}>R$ {this.props.val.note}</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text>{this.props.val.description}</Text>
                        <Text>{this.props.val.type}</Text>
                    </View>
                </View>
            );
        }
    }

    render() {
        return (
            <View key={this.props.keyVal} style={styles.note}>


                {this.renderCorEntradaOuSaida()}

                <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
                    <Text style={styles.noteDeleteText}>D</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    note: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },
    noteText: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#ad0000',
    },
    noteTextEntrada: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#068700',
    },
    noteDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10,
    },
    noteDeleteText: {
        color: 'white',
    }
});